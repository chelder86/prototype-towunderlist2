# README #

This is an unfinished Android app to send voice messages to Wunderlist translated to text. You can see the full architecture of the app in the wiki: https://bitbucket.org/chelder86/prototype-towunderlist2/wiki/Home

## Work DONE ##

If you start the app, a background service will be activated. No matter if you turn off the screen of your phone,** if you double push the power button, the app will detect it and start a new activity**.

## Work TO DO ##

I have been researching about how to implement it with Google Speech Recognition,  CMUSphinx and ATT Speech to Text. None of them convinced me because of different reasons: 

* Google Speech Recognition: it would only work if internet connection available. I wanted to make it work if no internet available too. The idea was that a background service will send the recorded voice to the Google server when internet connection came back. I found a way, but it was temporal: http://stackoverflow.com/questions/21470807/is-there-any-free-an-unlimited-speech-recognition-service-nowadays-which-allows
* CMUSphinx: useful to recognize a few words like "open Chrome", but creating a full dictionary with all the words of the alphabet is not a quick task at all.
*  ATT Speech to Text: maybe it could work if I pay. However I did not plan to get any money for this app, I just started it to learn Android, so paying was not an option for me.

## Alternative ##

I found the following apps the best option available to do the similar task:

* Detect double push of the power button (and other macros):  https://play.google.com/store/apps/details?id=com.incrediapp.press.it.macro.creator.time.saver
* Convert the voice to text and send to email (I think it does not work anymore): https://play.google.com/store/apps/details?id=it.RiccardoP.VoiceSearchStart

