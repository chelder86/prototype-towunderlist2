package cabesoft.prototype.towunderlist2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class ViewTasksNotSentYetActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_tasks_not_sent_yet);
	}

	
	// TODO Men�: muestra las distintas opciones (abrir otra actividad, salir...)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_tasks_not_sent_yet, menu);
		return true;
	}
	
	
	// TODO Men�: qu� hace al clickear en una opci�n (abre la actividad...)
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	  switch (item.getItemId()) {
	  case R.id.action_settings: 
		  startActivity(new Intent(this, SettingsActivity.class));
		  break;
	    
	  }
	  return true;
	} 	

}
