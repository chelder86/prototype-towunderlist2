package cabesoft.prototype.towunderlist2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * It is launched when power button has been pushed.
 * It checks if it has been a double push or not
 * @author ch
 *
 */
public class PowerButtonDoublePushCheckerBroadcastReceiver extends BroadcastReceiver{

	int timesPushed;
	float lastTime; 
	float currentTime; 
	float doubleClickDelay = 800; 
	
	/** 
	 * All time values in milliseconds
	 */
	public PowerButtonDoublePushCheckerBroadcastReceiver() {
		timesPushed = 0;
		lastTime = 0;			
	}
	
	
	/**
	 * If it detects double click on power button, it will start InputTextToSendToInboxActivity
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		currentTime = SystemClock.uptimeMillis();
		if (lastTime == -1
			|| currentTime - lastTime > doubleClickDelay){			
			timesPushed = 1;
			lastTime = currentTime;			
		}	
		else {	
			timesPushed = 2;
	        Intent i = new Intent(context, InputTextToSendToInboxActivity.class);
	        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	        i.putExtra("times_pushed", timesPushed);
	        context.startActivity(i);
	        timesPushed = 0;
	        lastTime = 0;	        
		}		
	}


	
}
