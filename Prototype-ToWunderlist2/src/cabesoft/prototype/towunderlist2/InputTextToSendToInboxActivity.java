package cabesoft.prototype.towunderlist2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public class InputTextToSendToInboxActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_input_text_to_send_to_inbox);
	}

	// TODO Men�: muestra las distintas opciones (abrir otra actividad, salir...)
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
		getMenuInflater().inflate(R.menu.input_text_to_send_to_inbox, menu);
		return true;
	}
	
	
	// TODO Men�: qu� hace al clickear en una opci�n (abre la actividad...)
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	  switch (item.getItemId()) {
	  case R.id.action_settings: 
		  startActivity(new Intent(this, SettingsActivity.class));
		  break;
	    
	  }
	  return true;
	} 
	

}
