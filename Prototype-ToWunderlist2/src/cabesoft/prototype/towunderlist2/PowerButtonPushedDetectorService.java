package cabesoft.prototype.towunderlist2;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class PowerButtonPushedDetectorService extends Service {

	BroadcastReceiver broadcastReceiver = null;
	
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO De momento utilizamos el onStart, pero quiz�s sea mejor utilizar este m�todo...
		return null;
	}
	
    @Override
    public void onCreate() {
        super.onCreate();         
//        Toast.makeText(getBaseContext(), "Service on create", Toast.LENGTH_SHORT).show();
        // Register receiver that detect screen on and screen off 
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        broadcastReceiver = new PowerButtonDoublePushCheckerBroadcastReceiver();
        registerReceiver(broadcastReceiver, filter);         
    }

    // FIXME Muestra las veces que se ha pulsado el powerbutton (lo recibe de PowerButtonDoublePushCheckerBroadcastReceiver)
    // FIXME Quiz�s haya que hacerlo con el onBind mejor
    @Override
    public void onStart(Intent intent, int startId) {          
        int timesPushed = 0; 
        try{
            // Get ON/OFF values sent from receiver ( AEScreenOnOffReceiver.java ) 
            //screenOn = intent.getBooleanExtra("screen_state", false);
            timesPushed = intent.getIntExtra("times_pushed", 0);  
            Toast.makeText(getBaseContext(), "Times pushed" + timesPushed, 
                    Toast.LENGTH_LONG).show();
        }catch(Exception e){}
         //  Toast.makeText(getBaseContext(), "Service on start :"+screenOn, 
                //Toast.LENGTH_SHORT).show(); 
    }
     
    @Override
    public void onDestroy() {         
//        Log.i("PowerButtonPushedDetectorService", "onDestroy");
        if(broadcastReceiver!=null)
         unregisterReceiver(broadcastReceiver);             
    }	
	

}
