package cabesoft.prototype.towunderlist2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


/** �sta es la actividad que se lanza cuando abrimos la app.
 *  Esta actividad NO tiene INTERFAZ de usuario, esto es,
 *  nunca se llama a setContentView() en onCreate().
 */ 
public class LauncherActivity extends Activity {
	
	/**
	 * It launches the services and the starting activity set on settings
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
// TODO Recoger opci�n de los Settings: dependiendo de las opciones y si ya se est� ejecutando el servicio o no, se llamar� a un servicio o a una actividad...
	    /**
	     * 1) InputTextToSendToInboxActivity
	     * 2) SettingsActivity
	     * 3) ViewTasksNotSentYetActivity
	     */	    
	    int startingActivity = 1;
	    
	    switch (startingActivity) {
	    case 1:	startActivity(new Intent(this, InputTextToSendToInboxActivity.class));
	    		break;
	    case 2: startActivity(new Intent(this, SettingsActivity.class));
	    		break;
	    case 3: startActivity(new Intent(this, ViewTasksNotSentYetActivity.class));
	    		break;
	    }	
	    
	   // It is always launched: the service itself will decide if stopping itself 
	   startService(new Intent(getBaseContext(), QueueToSendToWunderlistService.class));
	    
// TODO Recoger de las opciones si debe lanzarse el servicio de detectar el pulsado del power button o no:
	   boolean activatePowerButtonPushedDetectorService = true;
	   if (activatePowerButtonPushedDetectorService){
		   startService(new Intent(getBaseContext(), PowerButtonPushedDetectorService.class));
	   }
	}
}
